import os
from flask import Flask, request, jsonify
import requests
import json

app = Flask(__name__)

@app.route('/')
def getparamstring():
    district = request.args.get('district')
    computers = request.args.get('computers')
    wifi = request.args.get('wifi')
    param_string = ""
    token = os.environ.get('APP_TOKEN')
    url = "https://data.austintexas.gov/resource/tc36-hn4j.json?$$app_token=" + token
    if district is not None:
        param_string+="&district=" + district
    if computers is not None:
        param_string+="&computers=" + computers
    if wifi is not None:
        param_string+="&wifi=" + wifi

    if not param_string:
        default_data = "[{“welcome_message”: “Welcome to Austin Libraries Information Web Application”}]"
        return default_data
    else:
        url += param_string
        response = requests.get(url)
        json_data = response.json()

        json_list = []
        for i in json_data:
            data_list = {}
            data_list['human_address'] = i['address']['human_address']
            data_list['name'] = i['name']
            data_list['phone'] = i['phone']
            json_list.append(data_list)
        results = json.dumps(json_list)
        return results

if __name__ == '__main__':
    app.run(debug=True, port=5000)